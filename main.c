#include "mcc_generated_files/mcc.h"
#define LED_ON_TIME 55000
#define LED_OFF_TIME 110000

void regime1();
void regime2();
/*
                         Main application
 */
void main(void)
{
    uint32_t i;
    // Initialize the device
    SYSTEM_Initialize();
    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();
    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();
    /*INICIALIZA��O PARA CONSUMIR ENERGIA*/
    for( i = 0; i < 1000000; i++){
            ledMonitor_SetLow();
        }
    while (1)
    {
        /*PISCA LENTAMENTE*/
        if( sensorHal_GetValue() > 0 ){
            regime1();
        }
        else{
            /*PISCA MAIS R�PIDO*/
            while(1){
                regime2();
            }
        }
    }
}

/**
 * Neste regime o LED pisca rapidamente.
 */
void regime1(){
    static uint32_t counter1  = 0;
    if (++counter1 > LED_ON_TIME){
        ledMonitor_SetLow();
    }
    if (++counter1 > LED_OFF_TIME){
        ledMonitor_SetHigh();
        counter1 = 0;
    }
}

/**
 * Este regime � o segundo regime utilizado, nele o led pisca lentamentamente.
 */
void regime2(){
    static uint32_t counter  = 0;
    if (++counter > (LED_ON_TIME*10)){
        ledMonitor_SetHigh();
    }
    if (++counter > (LED_OFF_TIME*10)){
        ledMonitor_SetLow();
        counter = 0;
    }
}
/**
 End of File
*/